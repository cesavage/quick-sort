import quicksort.modules.data_validator as DataValidation
import quicksort.modules.appStrings as appStrings
import quicksort.modules.quicksorter as QuickSorter


def promptForUserInput():
    """ Prompts the user for input at the command line and returns the user input.
        
        This functionality is only broken out into its own method to improve readability
        of the main() method.
    """
    userInput = input("\nEnter a comma-separated list of integers. Integers will be returned in ascending order.\nType " + appStrings.EXIT_KEYWORD + " to exit.) Enter integers to sort: ")
    return userInput


def parseUserInput(userInput):
    """ 
    Parses the user's input by performing the following:
    1. Checking if the user is attempting to exit.
    2. Removes extra commas from the start of end of the string.
    3. Splits the user input string into a list at each ','.

    This functionality is only broken out into its own method to improve readability
    of the main() method.
        
    :param userInput: User input returned from the input() method.
    :type userInput: str

    :returns: list -- A list of the user's input, where that input was comma-delimited.
    """

    if userInput == appStrings.EXIT_KEYWORD:
        exit()

    userInput = DataValidation.DataValidator.cleanUserInput(userInput)
    userInput = userInput.split(',')
    return userInput



def validateUserInput(userInput):
    """
    Validates each element in the userInput list is an integer.
        
    :param userInput: Each element of the user's comma-separated input.
    :type userInput: list
        
    :returns: list -- A list of integers.
    """

    userInput = list( map(DataValidation.DataValidator.checkUserInputIsInt,userInput) )
    return userInput
    


def run():
    """
    The application's main() method. The entry point of the program.
    """

    while True:
        userInput = promptForUserInput()
        userInput = parseUserInput(userInput)

        try:
            userInput = validateUserInput(userInput)
        except DataValidation.ValidationException as dataValidationException:
            print(dataValidationException.message)
            continue

        quickSorter = QuickSorter.QuickSorter()

        print("\nSorted list: " + str( quickSorter.sortWithOutput(userInput, 0, len(userInput) - 1) ) )



if __name__ == "__main__":
    run()

