.. Quick Sort documentation master file, created by
   sphinx-quickstart on Sun Dec 29 16:48:54 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Quick Sort
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   algorithm
   concepts

* :ref:`modindex`
* :ref:`search`
