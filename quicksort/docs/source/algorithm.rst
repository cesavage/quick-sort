The Quick Sort Algorithm
========================

The Quick Sort algorithm is a sorting algorithm that leverages the optimal substructure of a sorting problem.

What is optimal substructure?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
In algorithmic thinking, a problem presents optimal substructure if it can be decomposed into smaller, solvable, problems *and* the solutions to those smaller problems can be combined to create a solution for the actual problem.

How does optimal substructure pertain to the quick-sort algorithm?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The quick sort algorithm relies on the idea that a list of one item is, by its essence, sorted. 

The algorithm sorts by beginning with a group of objects (in this case, a list of integers.) The algorithm then chooses one item from the list, referred to as the *pivot*, for which to find a sorted place in the list.

The algorithm moves iterates through each item in the list, placing all items smaller than the pivot on one side of the list, and all items larger than the array on the opposite side. When finished, the algorithm places the pivot value in between the values that are smaller than, and larger than, the pivot.

The algorithm then recurses, completing this same sort on the sublists that are both smaller and larger than the pivot. The algorithm continues to break these lists into sublists until only one item remains in each list. These one-item lists are trivially sorted, and they have been placed in sorted order in their containing list.

