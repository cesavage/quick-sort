import collections

class RecursionDisplayIndentor:
    """
    Provides a mechanism for expressing program recursion level as indented output.

    As the program executes, the developer may use the methods defined to indicate that a new recursion has taken place, or has been completed, and output an indention that corresponds to that level of recursion.

    The class is a singleton and tracks the recursion level over its entire scope.
    """
    __singleInstance = None
    
    @staticmethod
    def growIndent(): 
        """
        Increases the size of the indent to be output. Typically called when a new level of recursion has BEGUN.
        """  
        return None

    @staticmethod
    def shrinkIndent():
        """
        Decreases the size of the indent to be output. Typically called when an existing recursion has COMPLETED.
        """
        return None
    
    @staticmethod
    def printIndent(): 
        """
        Prints whitespace characters to indent the output that follows it by an amount that corresponds to the level of recursion that has been set by using the growIndent() and shrinkIndent() methods throughout the program.
        """
        return None

    

    def __init__(self):
        if RecursionDisplayIndentor.__singleInstance == None:
            RecursionDisplayIndentor.__singleInstance = self
            
            def definePrivateAttributesAndMethods(self):
                """
                Uses a closure to encapsulate the recursionCounter variable and private methods to access it.

                At the end of the method, these private methods are assigned to public class members providing access to the private attribute recursionCounter.
                """
                INDENT_SIZE = 2
                recursionCounter = 0
            
                @staticmethod
                def _increaseIndent():
                    nonlocal recursionCounter
                    recursionCounter += 1
                RecursionDisplayIndentor.growIndent = _increaseIndent
                
                @staticmethod
                def _decreaseIndent():
                    nonlocal recursionCounter
                    recursionCounter -= 1
                RecursionDisplayIndentor.shrinkIndent = _decreaseIndent

                @staticmethod
                def _printIndent():
                    return " " * INDENT_SIZE * recursionCounter
                RecursionDisplayIndentor.printIndent = _printIndent
            
            definePrivateAttributesAndMethods(self)