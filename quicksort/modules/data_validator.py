import quicksort.modules.appStrings as appStrings
import re as regex


class ValidationException(Exception):
    message="\nPlease enter only integers or '" + appStrings.EXIT_KEYWORD + "' to exit."



class DataValidator:
    """
    Class used for validation of user input.
    """
    @staticmethod
    def checkUserInputIsInt(inputValue):
        """
        Checks that the supplied parameteter is an integer.

        :param name: inputValue
        :param type: string
        
        :raises: ValidationException

        :returns: int -- the value from the input parameter cast as an integer.
        """
        try:
            intValue = int(inputValue)
        except ValueError:
            raise ValidationException()

        return intValue



    @staticmethod
    def cleanUserInput(userInput):
        """
        Removes extranneous commas from the start of end of the string parameter.

        :param name: userInput
        :param type: string

        :returns: str -- the input parameter with cleanup regex applied.
        """
        userInput = regex.sub( '^,|,$', '', userInput )
        
        return userInput