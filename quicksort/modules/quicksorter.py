from quicksort.modules.recursion_display_indentor import RecursionDisplayIndentor as Indentor

class QuickSorter():
    
    indentor = Indentor()   
    
    def sortWithOutput(self, integerList, startIndex, endIndex): 
        return 0

    def definePublicMethods(self):

        
        def _recursionIsBaseCase(startIndex, endIndex):
            """
            Checks if the recursion meets the base case condition of a list length <= 1.

            :param name: startIndex -- The starting index of the subarray used in this recursion.
            :param type: int

            :param name: endIndex -- The ending index of the subarray used in this recursion.
            :param type: int

            :returns: bool -- True, if the recursion meets the conditions of the base case.

            """
            if endIndex - startIndex < 1 :
                return True



        def _swapValuesAtIndices(integerList, index1, index2):
            """
            Swaps the values of two list indices.

            :param name: integerList -- the list in which to swap values.
            :param type: list

            :param name: index1 -- the index of one item to be swapped.
            :param type: int

            :param name: index2 -- the index of the other item to be swapped.
            :param type: int

            :returns: None 
            """
            integerListValue1 = integerList[index1]
            integerList[index1] = integerList[index2]
            integerList[index2] = integerListValue1
            return



        def _sortSublist(integerList, startIndex, endIndex, pivotValue, pivotSortedPosition):
            """
            Execute the quicksort algorithm on a sublist.

            :param name: integerList -- the complete list from which the sublist is derived.
            :param type: list

            :param name: startIndex -- the starting index of the sublist to be sorted.
            :param type: int

            :param name: endIndex -- the ending index of the sublist to be sorted.
            :param type: int

            :param name: pivotValue -- the value of the pivot against which sorting comparisons are made.
            :param type: int

            :param name: pivotSortedPosition -- the sublist index at which the pivot value shoudld
            be placed when the sort is complete.
            :param type: int

            :returns: int -- the array index of the sorted position of the pivot value.
            """
            for searchIndex in range(startIndex, endIndex):
                if integerList[searchIndex] < pivotValue:
                    pivotSortedPosition += 1
                    _swapValuesAtIndices(integerList, searchIndex, pivotSortedPosition)
            pivotSortedPosition += 1
            return pivotSortedPosition


        
        def sortWithOutput(self, integerList, startIndex, endIndex):
            """
            Executes quick sort and outputs status at each recursion.

            :param name: integerList -- The list of integers to be sorted
            type: list

            :param name: startIndex -- The starting index of the sublist to be sorted.
            :param type: int

            :param name: endIndex -- The ending index of the sublist to be sorted.
            :param type: int
            """
            sublistSlice = slice(startIndex, endIndex + 1)
            print("\n" + QuickSorter.indentor.printIndent() + "Sorting sublist: " + str(integerList[sublistSlice]) )
            if _recursionIsBaseCase(startIndex, endIndex):
                print(QuickSorter.indentor.printIndent() + "Base case reached.")
                QuickSorter.indentor.shrinkIndent()
                return

            pivotIndex = (endIndex - startIndex) // 2 + startIndex
            pivotValue = integerList[pivotIndex]
            pivotSortedPosition = startIndex -1

            print( QuickSorter.indentor.printIndent() + "Pivot value: " + str(pivotValue) )
            #Swap the pivot value to the end of the sublist, to get it out of the way during value comparisions.
            _swapValuesAtIndices(integerList, pivotIndex, endIndex)

            pivotSortedPosition = _sortSublist(integerList, startIndex, endIndex, pivotValue, pivotSortedPosition)

            #Swap the pivot value into the index directly after the end of the sublist of values
            #less than the pivot. This is it's sorted position.
            _swapValuesAtIndices(integerList, endIndex, pivotSortedPosition)
            print(QuickSorter.indentor.printIndent() + "Sorted sublist is: " + str(integerList[sublistSlice]))

            #Sort the sublists to the left and the right of properly-placed pivot.
            QuickSorter.indentor.growIndent()
            self.sortWithOutput(integerList, startIndex, pivotSortedPosition - 1)
            
            QuickSorter.indentor.growIndent()
            self.sortWithOutput(integerList, pivotSortedPosition + 1, endIndex)
                
            return integerList
        
        QuickSorter.sortWithOutput = sortWithOutput

    def __init__(self):
        self.definePublicMethods()