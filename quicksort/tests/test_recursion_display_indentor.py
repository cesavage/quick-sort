import unittest, re
from quicksort.modules.recursion_display_indentor import RecursionDisplayIndentor as Indentor

class test_indentor(unittest.TestCase):
    def test_printIndent(self):
        displayIndentor = Indentor()
        regex = "\S+"

        #Verify there are no non-whitespace characters in the output.
        result = bool( re.match(regex, displayIndentor.printIndent()) )
        self.assertEquals(result, False)



    def test_growIndent(self):
        displayIndentor = Indentor()
        startingIndentSize = len( displayIndentor.printIndent() )

        displayIndentor.growIndent()
        grownIndentSize = len( displayIndentor.printIndent() )

        self.assertTrue(startingIndentSize < grownIndentSize)



    def test_shrinkIndent(self):
        displayIndentor = Indentor()
        startingIndentSize = len( displayIndentor.printIndent() )

        displayIndentor.shrinkIndent()
        shrunkenIndentSize = len( displayIndentor.printIndent() )

        self.assertTrue(startingIndentSize > shrunkenIndentSize)