import unittest
from quicksort.modules.quicksorter import QuickSorter

class Test_quicksorter(unittest.TestCase):

    def test_sortWithOutput(self):
        integerList = [9,7,5,2,-12,3,0,8,6,4,1]

        quickSorter = QuickSorter()
        result = quickSorter.sortWithOutput(integerList, 0, ( len(integerList) - 1))

        self.assertEqual(result, [-12,0,1,2,3,4,5,6,7,8,9] )