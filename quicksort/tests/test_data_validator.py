import unittest
from quicksort.modules.data_validator import DataValidator, ValidationException

class TestDataValidator(unittest.TestCase):
    def test_checkUserInputIsInt(self):
        #Test validator accepts positive integer
        positiveInt = DataValidator.checkUserInputIsInt("5")
        self.assertEquals(positiveInt, 5)

        #Test validator accepts negative integer
        negativeInt = DataValidator.checkUserInputIsInt("-5")
        self.assertEquals(negativeInt, -5)

        #Test validator accepts 0
        zeroAsInt = DataValidator.checkUserInputIsInt("0")
        self.assertEquals(zeroAsInt, 0)

        #Test fractional values are not permitted.
        with self.assertRaises(ValidationException):
            DataValidator.checkUserInputIsInt("1.234")

        #Test non-numeric characters are not permitted.
        with self.assertRaises(ValidationException):
            DataValidator.checkUserInputIsInt("A")

    

    def test_cleanUserInput(self):
        #Test extra commas are removed from start and end of input string.
        result = DataValidator.cleanUserInput(",3,4,5,")
        self.assertEquals(result, "3,4,5")